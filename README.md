# Differ

> Differ is simple program that checks Swagger 2.0 spec URL periodicly
> and can report to Slack if configured properly.

## Installation

```
$ git clone https://gitlab.com/alexbenic/differ.git
$ ./gradlew jar
```

## Usage
```
$ java -jar build/libs/differ-VERSION.jar
```

## Contributing
- Follow [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0-beta.2/)
- Add yourself to `CONTRIBUTORS.md`
- Make a merge request

## License
[MIT](https://choosealicense.com/licenses/mit/)
