ARG VERSION=12.0.1

FROM openjdk:${VERSION}-jdk as BUILD

COPY . /src
WORKDIR /src
RUN ./gradlew --no-daemon jar

FROM openjdk:${VERSION}-jdk

COPY --from=BUILD /src/build/libs/differ-0.1.jar /bin/runner/differ.jar
WORKDIR /bin/runner

CMD ["java","-jar","differ.jar"]
