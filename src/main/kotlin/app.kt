package differ

import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result;
import com.deepoove.swagger.diff.SwaggerDiff
import com.deepoove.swagger.diff.output.MarkdownRender
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.Headers
import java.io.File
import java.util.logging.Logger

class Message(val msg: String) {
    fun json(): String {
        return "{ \"text\": \"$msg\" }"
    }
}

interface Reporter {
    fun report(message: String?)
}

class SlackReporter(private val settings: Settings) : Reporter {
    private val logger = Logger.getLogger(this.javaClass.name)

    override fun report(message: String?) {
        if (message.isNullOrBlank()) {
            return
        }

        val json = Message(message).json()
        this.logger.info("--> Sending message: $json")

        Fuel.post(settings.SLACK_HOOK)
            .header(Headers.CONTENT_TYPE, "application/json")
            .body(json)
            .response { _, _, result ->
                when (result) {
                    is Result.Failure ->
                        this.logger.info("--> failed to post to slack with error: " + result.getException().toString())
                    is Result.Success ->
                        this.logger.info("--> reported diff to slack :) ")
                }


            }
    }
}

class ConsoleReporter : Reporter {
    override fun report(message: String?) {
        if (message.isNullOrBlank()) {
            return
        }

        println(message)
    }
}

class Cache(private val location: String) {
    private val log = Logger.getLogger(this.javaClass.name)

    fun get(): String {
        val exists = File(location).exists()

        if (!exists) {
            return ""
        }

        return File(location).readText()
    }

    fun set(spec: String) {
        File(location).writeText(spec)
    }

    fun equals(spec: String): Boolean {
        return this.get().equals(spec)
    }

    val isEmpty: Boolean
        get() = this.get().equals("")
}

enum class Env {
    DEVELOPMENT,
    PRODUCTION
}

class Settings() {
    val ENV: Env
        get() {
            val env = System.getenv("ENV")

            if (env.isNullOrBlank()) {
                return Env.DEVELOPMENT
            }

            if (env.equals("production")) {
                return Env.PRODUCTION
            }

            return Env.DEVELOPMENT
        }
    val CACHE_LOCATION: String = System.getenv("CACHE_LOCATION") ?: "/tmp/cache.json"
    val DOCS_URL: String = System.getenv("DOCS_URL") ?: "http://localhost:3000/api/v1/docs/json"
    val REFRESH: Long
        get() {
            val env = System.getenv("REFRESH_TIMEOUT")
            val SEC = 1000L
            val MINUTE = 60L * SEC

//            if (!env.isNullOrBlank()) {
//                return (env * MINUTE).toLong()
//            }

            return 15 * SEC
        }
    val SLACK_HOOK: String =
        System.getenv("SLACK_HOOK") ?: "https://hooks.slack.com/services/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"
}

class App(private val settings: Settings, private val cache: Cache, private val reporter: Reporter) {
    private val log = Logger.getLogger(this.javaClass.name)

    private fun extract(result: Result<String, FuelError>): String {
        when (result) {
            is Result.Failure -> {
                throw result.getException()
            }
            is Result.Success -> {
                return result.get()
            }
        }

    }


    private fun fetch(url: String): Result<String, FuelError> {
        val (_, _, result) = url
            .httpGet()
            .responseString()

        return result
    }

    private fun compare(old: String, new: String): SwaggerDiff? {
        return SwaggerDiff.compareV2(old, new)
    }

    private fun render(diff: SwaggerDiff?): String? {
        return MarkdownRender().render(diff)
    }

    fun run() {
        while (true) {
            Thread.sleep(settings.REFRESH)
            log.info("--> Refreshing spec")

            try {

                val result = this.fetch(settings.DOCS_URL)

                val data = this.extract(result)

                if (cache.isEmpty) {
                    cache.set(data)
                    continue
                }

                if (cache.equals(data)) {
                    continue
                }

                log.info("--> New spec found. Diffing.")

                val md = this.render(this.compare(settings.CACHE_LOCATION, settings.DOCS_URL))

                cache.set(data)

                reporter.report(md)

            } catch (e: Error) {
                log.info("--> Cannot fetch doc: \n$e")
            }
        }
    }
}

fun reportFactory(settings: Settings): Reporter {
    if (settings.ENV.equals(Env.PRODUCTION)) {
        return SlackReporter(settings)
    }

    return ConsoleReporter()
}

fun main() {

    val settings = Settings()
    val cache = Cache(settings.CACHE_LOCATION)
    val reporter = reportFactory(settings)

    val app = App(settings, cache, reporter)

    app.run()
}

